#!/usr/bin/env bash

set -e
exec &> >(tee "00_prep.log")

print () {
    echo -e "\n\033[1m> $1\033[0m\n"
}

ask () {
    read -p "> $1 " -r
    echo
}

menu () {
    PS3="> Choose a number: "
    select i in "$@"
    do
        echo "$i"
        break
    done
}

select_disk () {
    select ENTRY in $(ls /dev/disk/by-id/);
    do
        DISK="/dev/disk/by-id/$ENTRY"
        echo "$DISK" > /tmp/disk
        echo "Installing on $ENTRY."
        break
    done
}

wipe () {
    ask "wipe all data on $ENTRY ?"
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        dd if=/dev/zero of="$DISK" bs=512 count=1
        wipefs -af "$DISK"
        sgdisk -Zo "$DISK"
    fi
}

partition() {
    sgdisk -n1:1M:+512M -t1:EF00 "$DISK"
    EFIP="$DISK-part1"

    sgdisk -n2:0:-8G -t2:bf01 "$DISK"
    ZFSP="$DISK-part2"

    partprobe "$DISK"
    sleep 1

    mkfs.vfat "$EFIP"
}

zfs_passphrase () {
    print "Set ZFS passphrase"
    read -r -p "> ZFS passphrase: " -s pass
    echo
    echo "$pass" > /etc/zfs/zroot.key
    chmod 000 /etc/zfs/zroot.key
}

create_pool () {
    print "Create ZFS pool"
    zpool create -f -o ashift=12                          \
                 -o autotrim=on                           \
                 -O acltype=posixacl                      \
                 -O compression=zstd                      \
                 -O relatime=on                           \
                 -O xattr=sa                              \
                 -O encryption=aes-256-gcm                \
                 -O keyformat=passphrase                  \
                 -O keylocation=file:///etc/zfs/zroot.key \
                 -O mountpoint=none                       \
                 -O canmount=off                          \
                 -R /mnt                                  \
                 zroot "$ZFSP"
}

create_root_dataset () {
    print "Create root dataset"
    zfs create -o mountpoint=none zroot/ROOT

    zfs set org.zfsbootmenu:commandline="ro quiet" zroot/ROOT
}

create_system_dataset () {
    print "Create slash dataset"
    zfs create -o mountpoint=/ -o canmount=noauto zroot/ROOT/"$1"

    print "Generate hostid"
    zgenhostid -f

    print "Set ZFS bootfs"
    zpool set bootfs="zroot/ROOT/$1" zroot

    zfs mount zroot/ROOT/"$1"
}

create_home_dataset () {
    echo "+--- Create misc datasets"
    zfs create -o mountpoint=/home                    zroot/home
    zfs create -o mountpoint=/var     -o canmount=off zroot/var
    zfs create                                        zroot/var/log
    zfs create -o mountpoint=/var/lib -o canmount=off zroot/var/lib
    zfs create                                        zroot/var/lib/libvirt
    zfs create                                        zroot/var/lib/docker
}

export_pool () {
    print "Export zpool"
    zpool export zroot
}

import_pool () {
    print "Import zpool"
    zpool import -d $ZFSP -R /mnt zroot -N -f
    zfs load-key zroot
}

mount_system () {
    print "Mount slash dataset"
    zfs mount zroot/ROOT/"$1"
    zfs mount -a

    print "Mount EFI part"
    EFI="$DISK-part1"
    mkdir -p /mnt/efi
    mount "$EFI" /mnt/efi
}

copy_zpool_cache () {
    print "Generate and copy zfs cache"
    mkdir -p /mnt/etc/zfs
    zpool set cachefile=/etc/zfs/zpool.cache zroot
}

print "Is this the first install or a second install to dualboot ?"
install_reply=$(menu first dualboot)

select_disk
zfs_passphrase

if [[ $install_reply == "first" ]]
then
    wipe
    partition
    create_pool
    create_root_dataset
fi

ask "Name of the slash dataset ?"
name_reply="$REPLY"
echo "$name_reply" > /tmp/root_dataset

if [[ $install_reply == "dualboot" ]]
then
    import_pool
fi

create_system_dataset "$name_reply"

if [[ $install_reply == "first" ]]
then
    create_home_dataset
fi

export_pool
import_pool
mount_system "$name_reply"
copy_zpool_cache

echo -e '\e[32mAll OK\033[0m'
